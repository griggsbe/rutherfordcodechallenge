﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RutherfordCodeChallenge.Beans
{
    [Serializable]
    class Event : INotifyPropertyChanged
    {
        private int? eventId;
        public int? EventID {
            get
            {
                return this.eventId;
            }
            set
            {
                this.eventId = value;
                NotifyPropertyChanged();
            }
        }

        private string eventName;
        public string EventName {
            get
            {
                return this.eventName;
            }
            set
            {
                this.eventName = value;
                NotifyPropertyChanged();
            }
        }


        private DateTime eventDate;
        public virtual DateTime EventDate {
            get
            {
                return this.eventDate;
            }
            set
            {
                this.eventDate = value;
                NotifyPropertyChanged();
            }
        }

        private string eventLocation;
        public string EventLocation {
            get
            {
                return this.eventLocation;
            }
            set
            {
                this.eventLocation = value;
                NotifyPropertyChanged();
            }
        }

        private double? ticketPrice;
        public double? TicketPrice {
            get
            {
                return this.ticketPrice;
            }
            set
            {
                if (value.ToString() == string.Empty)
                    this.ticketPrice = null;
                else
                    this.ticketPrice = value;
                NotifyPropertyChanged();
            }
        }

        private int? ticketsAvailable;
        public int? TicketsAvailable {
            get
            {
                return this.ticketsAvailable;
            }
            set
            {
                if (value.ToString() == string.Empty)
                    this.ticketsAvailable = null;
                else
                    this.ticketsAvailable = value;
                NotifyPropertyChanged();
            }
        }

        private string eventNotes;
        public string EventNotes {
            get
            {
                return this.eventNotes;
            }
            set
            {
                this.eventNotes = value;
                NotifyPropertyChanged();
            }
        }

        public Event()
        {
        }

        public Event(string eventName, DateTime eventDate, string eventLocation, double ticketPrice, int ticketsAvailable,
            string eventNotes)
        {
            this.EventName = eventName;
            this.EventDate = eventDate;
            this.EventLocation = eventLocation;
            this.TicketPrice = ticketPrice;
            this.TicketsAvailable = ticketsAvailable;
            this.EventNotes = eventNotes;
        }

        [field:NonSerializedAttribute()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
