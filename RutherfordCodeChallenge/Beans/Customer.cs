﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RutherfordCodeChallenge.Beans
{
    class Customer
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCreditCard { get; set; }
        public string CustomerEmail { get; set; }

        public Customer()
        {
        }

        public Customer(string name, string cc, string email)
        {
            this.CustomerName = name;
            this.CustomerCreditCard = cc;
            this.CustomerEmail = email;
        }

    }
}
