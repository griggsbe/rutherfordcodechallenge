﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RutherfordCodeChallenge.Beans
{
    class Purchase
    {
        public int PurchaseID { get; set; }
        public int EventID { get; set; }
        public int CustomerID { get; set; }
        public DateTime PurchaseDate { get; set; }
        public double PurchaseTotal { get; set; }
        public int TicketsPurchased { get; set; }
        public double TicketPrice { get; set; }

        public Purchase()
        {
        }

        public Purchase(Event e, Customer c, DateTime purchaseDate, int ticketQty)
        {
            this.EventID = (int)e.EventID;
            this.CustomerID = c.CustomerID;
            this.PurchaseDate = purchaseDate;
            this.PurchaseTotal = this.getPurchaseTotal((double)e.TicketPrice, ticketQty);
            this.TicketsPurchased = ticketQty;
            this.TicketPrice = (double)e.TicketPrice;
        }

        /*
         * Helper Methods
         */
         private double getPurchaseTotal(double price, int qty)
        {
            double total = 0;
            // 3% service charge on all purchases!
            total = (price * qty) * 1.03;
            return total;
        } 
    }
}
