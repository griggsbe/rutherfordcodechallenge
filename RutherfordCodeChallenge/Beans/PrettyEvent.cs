﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RutherfordCodeChallenge.Beans
{
    class PrettyEvent
    {
        public PrettyEvent(int eventId, string eventName, string eventDate, string eventLocation, double ticketPrice, int ticketsAvailable,
                string eventNotes)
        {
            this.EventID = eventId;
            this.EventName = eventName;
            this.EventDate = eventDate;
            this.EventLocation = eventLocation;
            this.TicketPrice = ticketPrice;
            this.TicketsAvailable = ticketsAvailable;
            this.EventNotes = eventNotes;
        }

        public int EventID { get; private set; }
        public string EventName { get; private set; }
        public string EventDate { get; private set; }
        public string EventLocation { get; private set; }
        public double TicketPrice { get; private set; }
        public int TicketsAvailable { get; private set; }
        public string EventNotes { get; private set; }
    }
}
