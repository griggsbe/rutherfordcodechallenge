﻿using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace RutherfordCodeChallenge.ViewModel
{
    public abstract class BaseViewModel : INotifyPropertyChanged, IDisposable
    {
        DelegateCommand closeCommand;
        DelegateCommand errorPopupOKCommand;

        protected BaseViewModel()
        {
        }

        public virtual string DisplayName { get; protected set; }

        //Property events
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                PropertyChangedEventArgs e = new PropertyChangedEventArgs(property);
                handler(this, e);
            }
        }

        //IDisposable required method
        public void Dispose()
        {
                //will want to dispose the database tool created in this class
                //dbTool.Dispose();
        }

        //close command to be shared amongst all views
        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null)
                {
                    closeCommand = new DelegateCommand(this.OnRequestClose, this.CanExecute);
                }
                return closeCommand;
            }
        }

        public event EventHandler RequestClose;

        private void OnRequestClose()
        {
            EventHandler handler = this.RequestClose;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected bool CanExecute()
        {
            return true;
        }

        //popup window properties and commands
        private Boolean isSuccessOpen;
        public Boolean IsSuccessOpen
        {
            get
            {
                return this.isSuccessOpen;
            }
            set
            {
                this.isSuccessOpen = value;
                this.OnPropertyChanged("IsSuccessOpen");
            }
        }

        private Boolean isErrorOpen;
        public Boolean IsErrorOpen
        {
            get
            {
                return this.isErrorOpen;
            }
            set
            {
                this.isErrorOpen = value;
                this.OnPropertyChanged("IsErrorOpen");
            }
        }

        private string errorText;
        public string ErrorText
        {
            get
            {
                if (this.errorText == null)
                {
                    return "";
                }
                return this.errorText;
            }
            set
            {
                this.errorText = value;
                this.OnPropertyChanged("ErrorText");
            }
        }

        private string viewSuccessText;
        public string ViewSuccessText
        {
            get
            {
                return this.viewSuccessText;
            }
            set
            {
                this.viewSuccessText = value;
                this.OnPropertyChanged("ViewSuccessText");
            }
        }

        public ICommand ErrorPopupOKCommand
        {
            get
            {
                if (errorPopupOKCommand == null)
                    errorPopupOKCommand = new DelegateCommand(this.errorBtnOK, this.CanExecute);
                return errorPopupOKCommand;
            }
        }

        private void errorBtnOK()
        {
            this.ErrorText = String.Empty;
            this.IsErrorOpen = false;
        }

    }
}
