﻿using Microsoft.Practices.Prism.Commands;
using RutherfordCodeChallenge.Beans;
using RutherfordCodeChallenge.Tools;
using RutherfordCodeChallenge.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace RutherfordCodeChallenge.ViewModel
{
    class MainWindowViewModel : BaseViewModel
    {
        DelegateCommand openViewEventsCommand;
        DelegateCommand openEditEventViewCommand;
        DelegateCommand generateReportCommand;
        DelegateCommand openEnterPurchaseCommand;
        DelegateCommand openAddCustomerCommand;
        DelegateCommand openAddEventViewCommand;

        ObservableCollection<BaseViewModel> viewTabs;

        private List<Event> events;
        private List<Customer> customers;
        private List<Purchase> sales;

        public MainWindowViewModel()
        {
        }

        //Binding resource for TabControl. Maintains all tab views currently present.
        public ObservableCollection<BaseViewModel> ViewTabs
        {
            get
            {
                if (viewTabs == null)
                {
                    viewTabs = new ObservableCollection<BaseViewModel>();
                    viewTabs.CollectionChanged += this.OnViewTabsChanged;
                }
                return viewTabs;
            }
        }

        //adds and removes the event for close requests to new and old tab items
        private void OnViewTabsChanged(object sender, NotifyCollectionChangedEventArgs e) 
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
            {
                foreach (BaseViewModel viewTab in e.NewItems)
                {
                    viewTab.RequestClose += this.OnViewTabRequestClose;
                }
            }

            if (e.OldItems != null && e.OldItems.Count != 0)
            {
                foreach (BaseViewModel viewTab in e.OldItems)
                {
                    viewTab.RequestClose -= this.OnViewTabRequestClose;
                }
            }
        } 

        private void OnViewTabRequestClose(object sender, EventArgs e)
        {
            BaseViewModel tabView = sender as BaseViewModel;
            tabView.Dispose();
            this.ViewTabs.Remove(tabView);
        }

        //Commands
        public ICommand OpenViewEventsCommand
        {
            get
            {
                if (this.openViewEventsCommand == null)
                {
                    openViewEventsCommand = new DelegateCommand(this.openViewEvents, base.CanExecute);
                }
                return openViewEventsCommand;
            }
        }

        public ICommand GenerateReportCommand
        {
            get
            {
                if (this.generateReportCommand == null)
                    generateReportCommand = new DelegateCommand(this.generateReport, base.CanExecute);
                return generateReportCommand;
            }
        }

        public ICommand OpenEditEventViewCommand
        {
            get
            {
                if (this.openEditEventViewCommand == null)
                    openEditEventViewCommand = new DelegateCommand(this.openEditEvent, base.CanExecute);
                return openEditEventViewCommand;
            }
        }

        public ICommand OpenEnterPurchaseViewCommand
        {
            get
            {
                if (this.openEnterPurchaseCommand == null)
                    openEnterPurchaseCommand = new DelegateCommand(this.openEnterPurchase, base.CanExecute);
                return openEnterPurchaseCommand;
            }
        }

        public ICommand OpenAddCustomerViewCommand
        {
            get
            {
                if (openAddCustomerCommand == null)
                    openAddCustomerCommand = new DelegateCommand(this.openAddCustomer, base.CanExecute);
                return openAddCustomerCommand;
            }
        }

        public ICommand OpenAddEventViewCommand
        {
            get
            {
                if (openAddEventViewCommand == null)
                    openAddEventViewCommand = new DelegateCommand(this.openAddEvent, base.CanExecute);
                return openAddEventViewCommand;
            }
        }

        /*
         * Open tab view actions
        */
        private void openAddEvent()
        {
            AddEventViewModel viewTab = this.ViewTabs.FirstOrDefault(vm => vm is AddEventViewModel)
                as AddEventViewModel;

            if (viewTab == null)
            {
                viewTab = new AddEventViewModel();
                this.ViewTabs.Add(viewTab);
            }
            this.SetActiveTab(viewTab);
        }

        private void openViewEvents()
        {
            //Limit 1 tab
            ViewEventsViewModel viewTab = this.ViewTabs.FirstOrDefault(vm => vm is ViewEventsViewModel)
               as ViewEventsViewModel;

            if (viewTab == null)
            {
                viewTab = new ViewEventsViewModel(this.retrieveEventList());
                this.ViewTabs.Add(viewTab);
            }
            this.SetActiveTab(viewTab);
        }

        private void openEditEvent()
        {
            EditEventViewModel viewTab = this.ViewTabs.FirstOrDefault(vm => vm is EditEventViewModel)
                as EditEventViewModel;

            if (viewTab == null)
            {
                viewTab = new EditEventViewModel(this.retrieveEventList());
                this.ViewTabs.Add(viewTab);
            }
            this.SetActiveTab(viewTab);
        }

        private void openEnterPurchase()
        {
            EnterPurchaseViewModel viewTab = this.ViewTabs.FirstOrDefault(vm => vm is EnterPurchaseViewModel)
                as EnterPurchaseViewModel;

            if (viewTab == null)
            {
                viewTab = new EnterPurchaseViewModel(this.retrieveEventList(), this.retrieveCustomerList());
                this.ViewTabs.Add(viewTab);
            }
            this.SetActiveTab(viewTab);
        }

        private void openAddCustomer()
        {
            AddCustomerViewModel viewTab = this.ViewTabs.FirstOrDefault(vm => vm is AddCustomerViewModel)
                as AddCustomerViewModel;

            if (viewTab == null)
            {
                viewTab = new AddCustomerViewModel();
                this.ViewTabs.Add(viewTab);
            }
            this.SetActiveTab(viewTab);
        }

        private List<Event> retrieveEventList()
        {
            try
            {
                return DatabaseTool.retrieveEventList();
            } catch (Exception ex)
            {
                base.ErrorText = "Error getting event list, please close and try again. Message: " + ex.Message;
                base.IsErrorOpen = true;
                return new List<Event>();
            }
        }

        private List<Customer> retrieveCustomerList()
        {
            try
            {
                return DatabaseTool.retrieveCustomerList();
            }
            catch (Exception ex)
            {
                base.ErrorText = "Error getting customer list, please close and try again. Message: " + ex.Message;
                base.IsErrorOpen = true;
                return new List<Customer>();
            }

        }

        private List<Purchase> retrieveSalesData()
        {
            try
            {
                return DatabaseTool.retrieveSalesReportData();
            }
            catch (Exception ex)
            {
                base.ErrorText = "Error getting sales data, please try again. Message: " + ex.Message;
                base.IsErrorOpen = true;
                return new List<Purchase>();
            }
        }

        private void generateReport()
        {
            this.sales = this.retrieveSalesData();
            this.events = this.retrieveEventList();
            this.customers = this.retrieveCustomerList();

            List<int> eventIds = new List<int>();
            foreach (Event e in events)
            {
                if (!eventIds.Contains((int)e.EventID))
                    eventIds.Add((int)e.EventID);
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("Event ID");
            sb.Append(",");
            sb.Append("Customer Name");
            sb.Append(",");
            sb.Append("Purchase Date");
            sb.Append(",");
            sb.Append("Purchase Total");
            sb.Append(",");
            sb.Append("Tickets Purchased");
            sb.Append(",");
            sb.Append("Per Ticket Price");
            sb.Append("\n");

            foreach (Purchase p in sales)
            {
                sb.Append(p.EventID);
                sb.Append(",");
                sb.Append(this.getCustomerName(p.CustomerID));
                sb.Append(",");
                sb.Append(p.PurchaseDate.ToString("MM/dd/yyyy"));
                sb.Append(",");
                sb.Append("$" + p.PurchaseTotal);
                sb.Append(",");
                sb.Append(p.TicketsPurchased);
                sb.Append(",");
                sb.Append("$" + p.TicketPrice);
                sb.Append("\n");
            }
            this.sendToCSVFile(sb);
        }

        private string getCustomerName(int id)
        {
            return ((Customer)this.customers.Find(x => x.CustomerID.Equals(id))).CustomerName.ToString();
        }

        private void sendToCSVFile(StringBuilder sb)
        {
            string fileName = @"C:\RutherfordApp\SalesReport-" + DateTime.Now.ToString("yyyy_MM_dd") + ".csv";
            Boolean success = true;
            try
            {
                System.IO.File.WriteAllText(fileName, sb.ToString());
            } catch (Exception e)
            {
                success = false;
                Console.WriteLine("Failed to write report to text  file. Msg: " + e.Message);
            }
            if (success)
            {
                base.ErrorText = "Sales Report generated and stored @ " + fileName + "!";
                base.IsErrorOpen = true;
            }
        }

        /*
         * Set new tab to active tab
        */
        private void SetActiveTab(BaseViewModel viewTab)
        {
            if (!this.ViewTabs.Contains(viewTab))
            {
                return;
            }

            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.ViewTabs);
            if (collectionView != null)
            {
                collectionView.MoveCurrentTo(viewTab);
            }
        }
    }
}
