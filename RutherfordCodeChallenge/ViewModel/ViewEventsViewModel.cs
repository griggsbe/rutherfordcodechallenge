﻿using Microsoft.Practices.Prism.Commands;
using RutherfordCodeChallenge.Beans;
using RutherfordCodeChallenge.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RutherfordCodeChallenge.ViewModel
{
    class ViewEventsViewModel : BaseViewModel
    {
        public List<PrettyEvent> Events { get; protected set; }

        public ViewEventsViewModel(List<Event> events)
        {
            base.DisplayName = "View Events";
            this.Events = this.convertToPrettyEventList(events);
        }

        private List<PrettyEvent> convertToPrettyEventList(List<Event> evl)
        {
            List<PrettyEvent> list = new List<PrettyEvent>();
            foreach (Event e in evl)
            {
                list.Add(new PrettyEvent((int)e.EventID, e.EventName, e.EventDate.ToShortDateString(), e.EventLocation, (double)e.TicketPrice, (int)e.TicketsAvailable, e.EventNotes));
            }
            return list;
        }
    }
}
