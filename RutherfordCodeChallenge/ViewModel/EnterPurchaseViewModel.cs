﻿using Microsoft.Practices.Prism.Commands;
using RutherfordCodeChallenge.Beans;
using RutherfordCodeChallenge.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RutherfordCodeChallenge.ViewModel
{
    class EnterPurchaseViewModel : BaseViewModel
    {
        public EnterPurchaseViewModel(List<Event> events, List<Customer> customers)
        {
            base.DisplayName = "Enter Purchase";
            this.ListEvents = events;
            this.ListCustomers = customers;
            this.NumberOfTickets = 0;
            base.ViewSuccessText = "Purchase successfully submitted!";
        }

        /*
         * Properties
         */
        DelegateCommand submitPurchaseCommand;

        private List<Event> events;
        public List<Event> ListEvents
        {
            get
            {
                return this.events;
            } set
            {
                this.events = value;
                base.OnPropertyChanged("ListEvents");
            }
        }

        private List<Customer> customers;
        public List<Customer> ListCustomers
        {
            get
            {
                return this.customers;
            }
            set
            {
                this.customers = value;
                base.OnPropertyChanged("ListCustomers");
            }
        }

        private int? purchaseQty;
        public int? NumberOfTickets
        {
            get
            {
                return this.purchaseQty;
            } set
            {
                this.purchaseQty = value;
                base.OnPropertyChanged("NumberOfTickets");
            }
        }

        private Event selectedEvent;
        public Event Event
        {
            get
            {
                return this.selectedEvent;
            }
            set
            {
                this.selectedEvent = value;
                base.OnPropertyChanged("Event");
            }
        }

        private Customer selectedCustomer;
        public Customer Customer
        {
            get
            {
                return this.selectedCustomer;
            } set
            {
                this.selectedCustomer = value;
                base.OnPropertyChanged("Customer");
            }
        }

        private string errorString;
        public string ErrorString
        {
            get
            {
                if (this.errorString == null)
                    return "";
                return this.errorString;
            } set
            {
                this.errorString = value;
                base.OnPropertyChanged("QuantityError");
            }
        }

        /*
         * Commands
         */
         public ICommand SubmitPurchaseCommand
        {
            get
            {
                if (this.submitPurchaseCommand == null)
                    this.submitPurchaseCommand = new DelegateCommand(this.submitCustomerPurchase, base.CanExecute);
                return this.submitPurchaseCommand;
            }
        }

        /*
         * Helper Methods
         */
        private void submitCustomerPurchase()
        {
            if (!this.isPurchaseDataValid())
            {
                base.IsErrorOpen = true;
                return;
            }
            Purchase p = new Purchase(this.Event, this.Customer, DateTime.Now, (int)this.NumberOfTickets);
            string result = DatabaseTool.insertNewPurchase(p);
            if (result == string.Empty)
            {
                base.IsSuccessOpen = true;
            } else
            {
                base.ErrorText = result;
                base.IsErrorOpen = true;
            }
        }
        
        private Boolean isPurchaseDataValid()
        {
            Boolean result = true;

            if (this.selectedEvent == null)
            {
                base.ErrorText += "\nNull event received, please select from the list.";
                result = false;
            }
            if (this.selectedCustomer == null)
            {
                base.ErrorText += "\nNull customer received, please select from the list.";
                result = false;
            }
            if (!result)
                return result;

            if (this.NumberOfTickets <= 0)
            {
                base.ErrorText += "\nTickets purchased must be greater than zero.";
                result = false;
            }

            return result;
        } 
    }
}
