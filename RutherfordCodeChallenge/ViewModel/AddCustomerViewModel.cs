﻿using Microsoft.Practices.Prism.Commands;
using RutherfordCodeChallenge.Beans;
using RutherfordCodeChallenge.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RutherfordCodeChallenge.ViewModel
{
    class AddCustomerViewModel : BaseViewModel
    {
        DelegateCommand submitNewCustomerCommand;

        public AddCustomerViewModel()
        {
            base.DisplayName = "Add Customer";
            base.ViewSuccessText = "Customer successfully added!";
        }

        /*
         * Properties
         */
        private string customerName;
         public string CustomerName
        {
            get
            {
                return this.customerName;
            }
            set
            {
                this.customerName = value;
                base.OnPropertyChanged("CustomerName");
            }
        }

        private string customerCreditCard;
        public string CustomerCreditCard
        {
            get
            {
                return this.customerCreditCard;
            } set
            {
                this.customerCreditCard = value;
                base.OnPropertyChanged("CustomerCreditCard");
            }
        }

        private string customerEmail;
        public string CustomerEmail
        {
            get
            {
                return this.customerEmail;
            }
            set
            {
                this.customerEmail = value;
                base.OnPropertyChanged("CustomerEmail");
            }
        }
        private string errorString;
        public string ErrorString
        {
            get
            {
                return this.errorString;
            }
            set
            {
                this.errorString = value;
                base.OnPropertyChanged("ErrorString");
            }
        }
             
        /*
         * Commands
         */
         public ICommand SubmitNewCustomerCommand
        {
            get
            {
                if (this.submitNewCustomerCommand == null)
                    submitNewCustomerCommand = new DelegateCommand(this.SubmitCustomerData, base.CanExecute);
                return submitNewCustomerCommand;
            }
        }

        /*
         * Helper Methods
         */
         private void SubmitCustomerData()
        {
            if (customerDataIsValid())
            {
                Customer c = new Customer(this.CustomerName.Trim(), this.CustomerCreditCard.Trim(), this.CustomerEmail.Trim());
                string result = DatabaseTool.insertNewCustomer(c);
                if (result == string.Empty)
                    base.IsSuccessOpen = true;
                else
                {
                    base.ErrorText = result;
                    base.IsErrorOpen = true;
                }
            } else
            {
                base.IsErrorOpen = true;
            }
        }

        private Boolean customerDataIsValid()
        {
            Boolean result = true;

            if (this.CustomerName == null)
            {
                base.ErrorText += "Null CustomerName received.\n";
                result = false;
            }
            if (this.CustomerCreditCard == null)
            {
                base.ErrorText += "Null CustomerCreditCard received.\n";
                result = false;
            }
            if (this.CustomerEmail == null)
            {
                base.ErrorText += "Null CustomerEmail received\n";
                result = false;
            }

            //if there was a null return now
            if (!result)
                return result;

            //check name
            if (!(Regex.IsMatch(this.CustomerName.Trim(), "[A-z ,.'-]*")))
            {
                base.ErrorText += "Customer Name not properly formatted.\n";
                result = false;
            }
            //check credit card number
            if (!(Regex.IsMatch(this.CustomerCreditCard.Trim(), "[0-9]{16}")))
            {
                base.ErrorText += "\nCustomer Credit Care not properly formatted. Only digits please!";
                result = false;
            }
            //check email
            try
            {
                MailAddress ma = new MailAddress(this.customerEmail.Trim());
            } catch (FormatException)
            {
                base.ErrorText += "\nCustomer email not properly formatted. Example: me@gmail.com";
                result = false;
            }
            return result;
        }


    }
}
