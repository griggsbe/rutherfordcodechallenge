﻿using Microsoft.Practices.Prism.Commands;
using RutherfordCodeChallenge.Beans;
using RutherfordCodeChallenge.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RutherfordCodeChallenge.ViewModel
{
    class EditEventViewModel : BaseViewModel
    {
        /*
         * Constructor
         */
         public EditEventViewModel(List<Event> events)
        {
            base.DisplayName = "Edit Event";
            this.Events = events;
            this.SelectedEvent = new Beans.Event();
            base.ViewSuccessText = "Event update successful!";
        }

        /*
         * Properties
         */
        DelegateCommand submitCommand;
        public List<Event> Events { get; set; }
        private Event originalEvent { get; set; }
        private Event selectedEvent;
        public Event SelectedEvent {
            get
            {
                return selectedEvent;
            }
            set
            {
                if (value == null)
                    return;
                if (value.TicketPrice.ToString() == string.Empty)
                    value.TicketPrice = null;
                if (value.TicketsAvailable.ToString() == string.Empty)
                    value.TicketsAvailable = null;
                selectedEvent = value;
                if (SelectedEvent != null)
                    originalEvent = (Event)DeepClone(selectedEvent);
                base.OnPropertyChanged("SelectedEvent");
                base.OnPropertyChanged("SelectedEvent.EventDate");
                base.OnPropertyChanged("SelectedEvent.EventLocation");
                base.OnPropertyChanged("SelectedEvent.PricePerTicket");
                base.OnPropertyChanged("SelectedEvent.TicketsAvailable");
                base.OnPropertyChanged("SelectedEvent.EventInfo");
            }
        }

        public DateTime TodaysDate
        {
            get
            {
                return DateTime.Now;
            }
        }

        /*
         * Commands
         */
         public ICommand SubmitEventInfoCommand
        {
            get
            {
                if (submitCommand == null)
                    submitCommand = new DelegateCommand(this.sendEventInfoToDB, base.CanExecute);
                return submitCommand;
            }
        } 

        /*
         * Helper methods
         */
         private void sendEventInfoToDB()
        {
            if (!IsEditDataValid())
            {
                base.IsErrorOpen = true;
                return;
            }

            if (SelectedEvent != null && SelectedEvent.EventID > 0)
            {
                if (hasChanges(originalEvent, SelectedEvent))
                {
                    string result = DatabaseTool.updateEvent(SelectedEvent);
                    if (result == string.Empty)
                    {
                        base.IsSuccessOpen = true;
                    } else
                    {
                        base.ErrorText = result;
                        base.IsErrorOpen = true;
                    }
                }
                else
                {
                    base.ErrorText = "No changes discovered. No update will be executed.";
                    base.IsErrorOpen = true;
                }
            }
        } 

        private Boolean IsEditDataValid()
        {
            Boolean result = true;
            if (this.SelectedEvent == null)
            {
                base.ErrorText += "\nNull event, please select from the list.";
                result = false;
            }
            if (this.SelectedEvent.EventDate == null || this.SelectedEvent.EventDate.Year == 1)
            {
                base.ErrorText += "\nNull event date received.";
                result = false;
            }
            if (this.SelectedEvent.EventLocation == null || this.SelectedEvent.EventLocation == string.Empty)
            {
                base.ErrorText += "\nNull event location received.";
                result = false;
            }
            if (this.SelectedEvent.TicketsAvailable == null)
            {
                base.ErrorText += "\nNull tickets available received.";
                result = false;
            }
            if (this.SelectedEvent.TicketPrice == null)
            {
                base.ErrorText += "\nNull price per ticket received.";
                result = false;
            }
            if (this.SelectedEvent.EventNotes == null)
            {
                this.SelectedEvent.EventNotes = string.Empty;
            }

            if (!result)
                return result;

            if (this.SelectedEvent.TicketsAvailable < 0)
            {
                base.ErrorText += "Tickets available cannot be below 0, please correcet.";
                result = false;
            }
            if (this.SelectedEvent.TicketPrice < 0)
            {
                base.ErrorText += "Price per ticket cannot be below 0, please correct.";
                result = false;
            }

            return result;
        }

        private Boolean hasChanges(Event o, Event n)
        {
            //probably should alert issue here
            if (o.EventID != n.EventID)
            {
                base.ErrorText = "Two different EventIDs found: " + o.EventID + "/" + n.EventID + ". Cancelling update. Please close and retry.";
                base.IsErrorOpen = true;
                return false;
            }

            if (o.EventName != n.EventName)
                return true;
            if (o.EventDate != n.EventDate)
                return true;
            if (o.EventLocation != n.EventLocation)
                return true;
            if (o.TicketPrice != n.TicketPrice)
                return true;
            if (o.TicketsAvailable != n.TicketsAvailable)
                return true;
            if (o.EventNotes != n.EventNotes)
                return true;

            return false;
        }

        private static object DeepClone(object obj)
        {
            object objResult = null;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);

                ms.Position = 0;
                objResult = bf.Deserialize(ms);
            }
            return objResult;
        }
    }
}
