﻿using Microsoft.Practices.Prism.Commands;
using RutherfordCodeChallenge.Beans;
using RutherfordCodeChallenge.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RutherfordCodeChallenge.ViewModel
{
    class AddEventViewModel : BaseViewModel
    {
        DelegateCommand submitNewEventCommand;

        public AddEventViewModel()
        {
            base.DisplayName = "Add Event";
            base.ViewSuccessText = "Event successfully added!";
        }

        /*
         * Properties
         */
        private string eventName;
        public string EventName
        {
            get
            {
                return this.eventName;
            } set
            {
                this.eventName = value;
                base.OnPropertyChanged("EventName");
            }
        }

        private DateTime eventDate;
        public DateTime EventDate
        {
            get
            {
                return this.eventDate;
            }
            set
            {
                this.eventDate = value;
                base.OnPropertyChanged("EventDate");
            }
        }

        private string eventLocation;
        public string EventLocation
        {
            get
            {
                return this.eventLocation;
            } set
            {
                this.eventLocation = value;
                base.OnPropertyChanged("EventLocation");
            }
        }

        private double? pricePerTicket;
        public double? PricePerTicket
        {
            get
            {
                return this.pricePerTicket;
            } set
            {
                this.pricePerTicket = value;
                base.OnPropertyChanged("PricePerTicket");
            }
        }

        private int? ticketsAvailable;
        public int? TicketsAvailable
        {
            get
            {
                return this.ticketsAvailable;
            }
            set
            {
                this.ticketsAvailable = value;
                base.OnPropertyChanged("TicketsAvailable");
            }
        }

        private string eventNotes;
        public string EventNotes
        {
            get
            {
                return this.eventNotes;
            }
            set
            {
                this.eventNotes = value;
                base.OnPropertyChanged("EventNotes");
            }
        }

        public DateTime TodaysDate
        {
            get
            {
                return DateTime.Now;
            }
        }

        /*
         * Commands
         */
        public ICommand SubmitNewEventCommand
        {
            get
            {
                if (submitNewEventCommand == null)
                    submitNewEventCommand = new DelegateCommand(this.insertNewEvent, base.CanExecute);
                return submitNewEventCommand;
            }
        }

        /*
         * Helper methods
         */
         private void insertNewEvent()
        {
            if (!this.validateNewEventData())
            {
                this.IsErrorOpen = true;
                return;
            }

            Event e = new Event(this.EventName, this.EventDate, this.EventLocation, (double)this.PricePerTicket, (int)this.TicketsAvailable, this.EventNotes);
            string result = DatabaseTool.insertNewEvent(e);
            if (result == string.Empty)
            {
                this.IsSuccessOpen = true;
            } else
            {
                this.ErrorText = result;
                this.IsErrorOpen = true;
            }
        }

        private Boolean validateNewEventData()
        {
            Boolean result = true;
            base.ErrorText = "";

            if (this.EventName == null || this.EventName == string.Empty)
            {
                base.ErrorText += "\nNull event name received.";
                result = false;
            }
            if (this.EventDate == null || this.EventDate.Year == 1)
            {
                base.ErrorText += "\nNull event date received.";
                result = false;
            }
            if (this.EventLocation == null || this.EventLocation == string.Empty)
            {
                base.ErrorText += "\nNull event location received.";
                result = false;
            }
            if (this.TicketsAvailable == null)
            {
                base.ErrorText += "\nNull tickets available received.";
                result = false;
            }
            if (this.PricePerTicket == null)
            {
                base.ErrorText += "\nNull price per ticket received.";
                result = false;
            }
            if (this.EventNotes == null)
            {
                this.EventNotes = string.Empty;
            }

            //return if any null
            if (!result)
                return result;

            //check price per ticket, must be greater than or equal to 0
            if (this.PricePerTicket < 0)
            {
                base.ErrorText = base.ErrorText + "\nPrice per ticket invalid. Must be greater than or equal to 0.";
                result = false;
            }

            //check tickets available, must be greater than 0
            if (this.TicketsAvailable <= 0)
            {
                base.ErrorText = base.ErrorText + "\nTickets available invalid. Must be greater than 0.";
                result = false;
            }

            return result;
        } 
    }
}
