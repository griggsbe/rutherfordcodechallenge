﻿using RutherfordCodeChallenge.Beans;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RutherfordCodeChallenge.Tools
{
    static class DatabaseTool
    {
        private static string CONNECTION_STRING = "data source=(local)\\MYSQLDB;Database=RutherfordAppDB;Integrated Security=True;";

        /*
         * Data Retrieval Methods 
         */
        public static List<Event> retrieveEventList()
        {
            List<Event> list = null;
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT * FROM EVENTS";

                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataTable dt = new DataTable("EVENTS");
                    sda.Fill(dt);

                    list = new List<Event>();
                    foreach(DataRow row in dt.Rows)
                    {
                        Event e = new Event()
                        {
                            EventID = Convert.ToInt32(row["eventId"]),
                            EventName = Convert.ToString(row["name"]),
                            EventDate = Convert.ToDateTime(row["date"]),
                            EventLocation = Convert.ToString(row["location"]),
                            TicketPrice = Convert.ToDouble(row["price"]),
                            TicketsAvailable = Convert.ToInt32(row["available_tickets"]),
                            EventNotes = Convert.ToString(row["notes"])
                        };
                        list.Add(e);
                    }
                }
            } catch (Exception e)
            {
                throw new Exception("Failed to retrieve events from database. Message: " + e.Message);
            } finally
            {
                if (con != null)
                    con.Close();
            }
            return list;
        }

        public static List<Customer> retrieveCustomerList()
        {
            List<Customer> list = null;
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT * FROM CUSTOMERS";

                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataTable dt = new DataTable("CUSTOMERS");
                    sda.Fill(dt);

                    list = new List<Customer>();
                    foreach (DataRow row in dt.Rows)
                    {
                        Customer c = new Customer()
                        {
                            CustomerID = Convert.ToInt32(row["customerId"]),
                            CustomerName = Convert.ToString(row["name"]),
                            CustomerCreditCard = Convert.ToString(row["creditCard"]),
                            CustomerEmail = Convert.ToString(row["email"]),
                        };
                        list.Add(c);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Failed to retrieve customers from database. Message: " + e.Message);
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return list;
        }

        public static List<Purchase> retrieveSalesReportData()
        {
            List<Purchase> list = null;
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT * FROM PURCHASES";

                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataTable dt = new DataTable("PURCHASES");
                    sda.Fill(dt);

                    list = new List<Purchase>();
                    foreach (DataRow row in dt.Rows)
                    {
                        Purchase p = new Purchase()
                        {
                            PurchaseID = Convert.ToInt32(row["purchaseId"]),
                            EventID = Convert.ToInt32(row["eventId"]),
                            CustomerID = Convert.ToInt32(row["customerId"]),
                            PurchaseDate = Convert.ToDateTime(row["purchase_date"]),
                            PurchaseTotal = Convert.ToDouble(row["total"]),
                            TicketsPurchased = Convert.ToInt32(row["tickets_purchased"]),
                            TicketPrice = Convert.ToDouble(row["ticket_price"])
                        };
                        list.Add(p);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Failed to retrieve purchase data from database. Message: " + e.Message);
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return list;
        }

        private static int getEventTicketsRemaining(int eventId)
        {
            int? currentTicketCount = null;
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT available_tickets FROM EVENTS WHERE eventId = @eventId";
                    command.Parameters.AddWithValue("@eventId", eventId);

                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataTable dt = new DataTable("EVENTS");
                    sda.Fill(dt);

                    foreach(DataRow row in dt.Rows)
                    {
                        return Convert.ToInt32(row["available_tickets"]);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Failed to retrieve tickets remaining from database. Message: " + e.Message);
            }
            finally
            {
                if (con != null)
                    con.Close();
            }

            return (int)currentTicketCount;
        }

        /*
         * New Data Submission Methods
         */
         public static string insertNewEvent(Event e)
        {
            string error = String.Empty;
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "INSERT INTO EVENTS(name, date, location, price, available_tickets, notes)" +
                                            " VALUES(@name, @date, @location, @price, @available_tickets, @notes)";
                    command.Parameters.AddWithValue("@name", e.EventName);
                    command.Parameters.AddWithValue("@date", e.EventDate.Date);
                    command.Parameters.AddWithValue("@location", e.EventLocation);
                    command.Parameters.AddWithValue("@price", e.TicketPrice);
                    command.Parameters.AddWithValue("@available_tickets", e.TicketsAvailable);
                    command.Parameters.AddWithValue("@notes", e.EventNotes);

                    command.ExecuteNonQuery();
                }
            } catch (Exception ex)
            {
                error = "Exception during insert event, message: " + ex.Message;
            } finally
            {
                if (con != null)
                    con.Close();
            }
            return error;
        }

        public static string insertNewCustomer(Customer c)
        {
            string error = String.Empty;
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "INSERT INTO CUSTOMERS(name, creditCard, email)" +
                                            " VALUES(@name, @creditCard, @email)";
                    command.Parameters.AddWithValue("@name", c.CustomerName);
                    command.Parameters.AddWithValue("@creditCard", c.CustomerCreditCard);
                    command.Parameters.AddWithValue("@email", c.CustomerEmail);

                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                error = "Exception during insert customer, message: " + ex.Message;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return error;
        }

        public static string insertNewPurchase(Purchase p)
        {
            string error = String.Empty;
            SqlConnection con = null;
            try
            {
                int ticketsRemaining = getEventTicketsRemaining(p.EventID);
                if (ticketsRemaining < p.TicketsPurchased)
                {
                    error = "Not enough tickets remaining. Left: " + ticketsRemaining + ", Purchase Amount: " + p.TicketsPurchased;
                    return error;
                }

                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "INSERT INTO PURCHASES(eventId, customerId, purchase_date, total, tickets_purchased, ticket_price)" +
                                            " VALUES(@eventId, @customerId, @purchase_date, @total, @tickets_purchased, @ticket_price)";
                    command.Parameters.AddWithValue("@eventId", p.EventID);
                    command.Parameters.AddWithValue("@customerId", p.CustomerID);
                    command.Parameters.AddWithValue("@purchase_date", p.PurchaseDate);
                    command.Parameters.AddWithValue("@total", p.PurchaseTotal);
                    command.Parameters.AddWithValue("@tickets_purchased", p.TicketsPurchased);
                    command.Parameters.AddWithValue("@ticket_price", p.TicketPrice);

                    command.ExecuteNonQuery();
                }
                error = decrementEventTickets(p.EventID, (ticketsRemaining-p.TicketsPurchased));
            }
            catch (Exception ex)
            {
                error = "Exception during insert purchase, message: " + ex.Message;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return error;
        }

        /*
         * Update Methods
         */ 
         public static string updateEvent(Event e)
        {
            string error = String.Empty;
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "UPDATE EVENTS SET NAME = @name, DATE = @date, LOCATION = @location, "
                                        + "PRICE = @price, AVAILABLE_TICKETS = @ticketsAvailable, NOTES = @notes " +
                                            "WHERE EVENTID = @eventId";
                    command.Parameters.AddWithValue("@name", e.EventName);
                    command.Parameters.AddWithValue("@date", e.EventDate.Date);
                    command.Parameters.AddWithValue("@location", e.EventLocation);
                    command.Parameters.AddWithValue("@price", e.TicketPrice);
                    command.Parameters.AddWithValue("@ticketsAvailable", e.TicketsAvailable);
                    command.Parameters.AddWithValue("@notes", e.EventNotes);
                    command.Parameters.AddWithValue("@eventId", e.EventID);
                    int rowsAffected = command.ExecuteNonQuery();
                    if (rowsAffected > 1)
                        error = "Command was successful but more than one event was updated! This shouldn't happen!";
                    if (rowsAffected == 0)
                        error = "No rows were affected by the update.";
                }
            }
            catch (Exception ex)
            {
                error = "Update Event failed. Message: " + ex.Message;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return error;
        }

        private static string decrementEventTickets(int eventId, int newQty)
        {
            string error = String.Empty;
            SqlConnection con = null;
            try
            {
                using (con = new SqlConnection(CONNECTION_STRING))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = con;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "UPDATE EVENTS SET AVAILABLE_TICKETS = @ticketsAvailable " +
                                            "WHERE EVENTID = @eventId";
                    command.Parameters.AddWithValue("@ticketsAvailable", newQty);
                    command.Parameters.AddWithValue("@eventId", eventId);
                    int rowsAffected = command.ExecuteNonQuery();
                    if (rowsAffected > 1)
                        error = "[DecrementEventTickets]Command was successful but more than one event was updated! This shouldn't happen!";
                    if (rowsAffected == 0)
                        error = "[DecrementEventTickets]No rows were affected by the update.";
                }
            }
            catch (Exception ex)
            {
                error = "[DecrementEventTickets]Update Event failed. Message: " + ex.Message;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return error;
        }
    }
}
