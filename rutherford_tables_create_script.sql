/* 	
    Execute against local sql server.
*/

CREATE DATABASE RutherfordAppDB
GO

USE RutherfordAppDB
GO

CREATE TABLE CUSTOMERS (
    customerId INT           IDENTITY (1, 1) NOT NULL,
    name       VARCHAR (100) NULL,
    creditCard VARCHAR (19)  NULL,
    email      VARCHAR (100) NULL
);

CREATE TABLE EVENTS (
    eventId           INT           IDENTITY (1, 1) NOT NULL,
    name              VARCHAR (250) NULL,
    date              DATETIME      NULL,
    location          VARCHAR (250) NULL,
    price             MONEY         NULL,
    available_tickets INT           NULL,
    notes             VARCHAR (500) NULL
);

CREATE TABLE PURCHASES (
    purchaseId        INT      IDENTITY (1, 1) NOT NULL,
    eventId           INT      NULL,
    customerId        INT      NULL,
    purchase_date     DATETIME NULL,
    total             MONEY    NULL,
    tickets_purchased INT      NULL,
    ticket_price      MONEY    NULL
);